import { NotFoundException } from '@nestjs/common'
import { Injectable, NestMiddleware } from '@nestjs/common'
import { NextFunction, Request } from 'express'
import { students } from '../../db'

@Injectable()
export class ValidStudentMiddleware implements NestMiddleware {
  use(req: Request, res: Response, next: NextFunction) {
    const studentId = req.params.id
    const studentExists = students.some((student) => student.id === studentId)

    if (!studentExists)
      throw new NotFoundException('Student with provided ID was not found')

    next()
  }
}
