import {
  MiddlewareConsumer,
  Module,
  NestModule,
  RequestMethod
} from '@nestjs/common'
import { ValidStudentMiddleware } from 'src/common/middlewares/validStudent.middleware'
import { StudentsController } from './students.controller'
import { StudentsService } from './students.service'

@Module({
  controllers: [StudentsController],
  providers: [StudentsService],
  exports: [StudentsService]
})
export class StudentsModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(ValidStudentMiddleware).forRoutes(
      {
        path: 'students/:id',
        method: RequestMethod.GET
      },
      { path: 'students/:id', method: RequestMethod.PUT }
    )
  }
}
