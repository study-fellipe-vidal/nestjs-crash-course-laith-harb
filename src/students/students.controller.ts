import { Body, Get, Param, ParseUUIDPipe, Post, Put } from '@nestjs/common'
import { Controller } from '@nestjs/common'
import {
  CreateStudentDto,
  FindStudentResponseDto,
  StudentResponseDto,
  UpdateStudentDto
} from './dto/student.dto'
import { StudentsService } from './students.service'

@Controller('students')
export class StudentsController {
  constructor(private readonly studentsService: StudentsService) {}

  @Get()
  getStudents(): FindStudentResponseDto[] {
    return this.studentsService.getStudents()
  }

  @Get(':id')
  getStudentById(
    @Param('id', new ParseUUIDPipe()) id: string
  ): FindStudentResponseDto {
    return this.studentsService.getStudentById(id)
  }

  @Post()
  createStudent(@Body() body: CreateStudentDto): StudentResponseDto {
    return this.studentsService.createStudent(body)
  }

  @Put(':id')
  updateStudent(
    @Param('id', new ParseUUIDPipe()) id: string,
    @Body() body: UpdateStudentDto
  ): StudentResponseDto {
    return this.studentsService.updateStudent(id, body)
  }
}
