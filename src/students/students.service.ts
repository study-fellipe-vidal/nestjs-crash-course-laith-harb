import { Injectable } from '@nestjs/common'
import { students } from 'src/db'
import {
  CreateStudentDto,
  FindStudentResponseDto,
  StudentResponseDto,
  UpdateStudentDto
} from './dto/student.dto'
import { v4 as uuidv4 } from 'uuid'

@Injectable()
export class StudentsService {
  private students = students

  getStudents(): FindStudentResponseDto[] {
    return this.students
  }

  getStudentById(id: string): FindStudentResponseDto {
    return this.students.find((student) => student.id === id)
  }

  createStudent(student: CreateStudentDto): StudentResponseDto {
    const newStudent = {
      id: uuidv4(),
      ...student
    }

    this.students.push(newStudent)

    return newStudent
  }

  updateStudent(id: string, payload: UpdateStudentDto): StudentResponseDto {
    let updatedStudent: StudentResponseDto

    const updatedStudentsList = this.students.map((student) => {
      if (student.id === id) {
        updatedStudent = {
          id: student.id,
          ...payload
        }
        return updatedStudent
      }
      return student
    })

    this.students = updatedStudentsList

    return updatedStudent
  }

  getStudentsByTeacherId(id: string): FindStudentResponseDto[] {
    return this.students.filter((student) => student.teacher === id)
  }

  updateStudentTeacher(
    studentId: string,
    teacherId: string
  ): StudentResponseDto {
    let updatedStudent: StudentResponseDto

    const updatedStudentsList = this.students.map((student) => {
      if (student.id === studentId) {
        updatedStudent = {
          ...student,
          teacher: teacherId
        }
        return updatedStudent
      }
      return student
    })

    this.students = updatedStudentsList

    return updatedStudent
  }
}
