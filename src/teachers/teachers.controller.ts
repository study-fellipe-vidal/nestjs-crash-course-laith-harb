import { Param, ParseUUIDPipe } from '@nestjs/common'
import { Get } from '@nestjs/common'
import { Controller } from '@nestjs/common'
import { FindTeacherResponseDto } from './dto/teacher.dto'
import { TeachersService } from './teachers.service'

@Controller('teachers')
export class TeachersController {
  constructor(private readonly teachersService: TeachersService) {}

  @Get()
  getTeachers(): FindTeacherResponseDto[] {
    return this.teachersService.getTeachers()
  }

  @Get(':id')
  getTeacherById(
    @Param('id', new ParseUUIDPipe()) id: string
  ): FindTeacherResponseDto {
    return this.teachersService.getTeacherById(id)
  }
}
