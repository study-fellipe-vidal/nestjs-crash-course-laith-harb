import { Controller, Get, Param, ParseUUIDPipe, Put } from '@nestjs/common'
import {
  FindStudentResponseDto,
  StudentResponseDto
} from 'src/students/dto/student.dto'
import { StudentsService } from 'src/students/students.service'

@Controller('teachers/:teacherId/students')
export class StudentsTeachersController {
  constructor(private readonly studentsService: StudentsService) {}

  @Get()
  getStudents(
    @Param('teacherId', new ParseUUIDPipe()) id: string
  ): FindStudentResponseDto[] {
    return this.studentsService.getStudentsByTeacherId(id)
  }

  @Put(':studentId')
  updateStudentTeacher(
    @Param('teacherId', new ParseUUIDPipe()) teacherId: string,
    @Param('studentId', new ParseUUIDPipe()) studentId: string
  ): StudentResponseDto {
    return this.studentsService.updateStudentTeacher(studentId, teacherId)
  }
}
